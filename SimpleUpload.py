#!/usr/bin/env python3

# SimpleUpload is a cli tool to upload files to XMPP servers with HTTP Upload
# Copyright (C) 2016   <drymer@autistici.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import configparser
import argparse
import getpass
import logging
from sleekxmpp.xmlstream.stanzabase import ElementBase
from sleekxmpp.stanza.iq import Iq
from os.path import getsize
from xml.dom import minidom
from sys import argv, exit
from os.path import abspath

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(levelname)s] %(message)s')
handler = logging.StreamHandler()
handler.setFormatter(formatter)
log.addHandler(handler)

try:
    import sleekxmpp
except:
    log.error("Tienes que instalar sleekxmpp.")
    exit()

try:
    import requests
except:
    log.error("Tienes que instalar requests.")
    exit()


class Request(ElementBase):
    namespace = 'urn:xmpp:http:upload'
    name = 'request'
    plugin_attrib = 'request'
    interfaces = set(('filename', 'size'))
    sub_interfaces = interfaces


class xmpp(sleekxmpp.ClientXMPP):
    def __init__(self, jid, password, u_file):
        """ Declaración de algunas variables para usar en otras funciones. """
        sleekxmpp.ClientXMPP.__init__(self, jid, password)
        self.add_event_handler("session_start", self.start)
        self.jid = jid
        self.password = password
        self.u_file = u_file

    def start(self, event):
        """ Inicio. """

        self.use_tls = True
        self.get_roster()
        self.send_presence()
        self.register_stanza_plugin(Iq, CustomStanza)

    def discovery(self):
        """
        Pide que componentes tiene activos el servidor XMPP y comprueba si
        alguno de ellos es el de HTTP Upload. Si lo encuentra, lo devuelve,
        sinó devuelve un string vacío.
        """

        disco = sleekxmpp.basexmpp.BaseXMPP.Iq(self)
        disco['query'] = "http://jabber.org/protocol/disco#items"
        disco['type'] = 'get'
        disco['from'] = self.jid
        disco['to'] = self.jid.split('@')[1]
        d = disco.send(timeout=30)

        xml = minidom.parseString(str(d))
        item = xml.getElementsByTagName('item')

        for component in item:
            component = component.getAttribute('jid')
            info = self.disco_info(component)

            if "urn:xmpp:http:upload" in info:
                http_upload_component = component
                break
            else:
                http_upload_component = ""

        return http_upload_component

    def disco_info(self, component):
        """ Pide información acerca de lo que ofrecen los componentes. """

        info = sleekxmpp.basexmpp.BaseXMPP.Iq(self)
        info['query'] = "http://jabber.org/protocol/disco#info"
        info['type'] = 'get'
        info['from'] = self.jid
        info['to'] = component

        return str(info.send(timeout=30))

    def upload(self, component, verify_ssl, max_size):
        """
        Recibe el nombre del dominio del componente HTTP Upload, sube el
        archivo y imprime por pantalla el link al que se ha subido.
        """

        peticion = Request()
        peticion['size'] = str(getsize(self.u_file))

        if int(peticion['size']) > int(max_size):
            log.error('El archivo que intentas subir es demasiado grande.')
            self.disconnect()
            exit()

        peticion['filename'] = self.u_file.split('/')[-1]

        iq = sleekxmpp.basexmpp.BaseXMPP.Iq(self)
        iq.set_payload(peticion)
        iq['type'] = 'get'
        iq['to'] = component
        iq['from'] = self.jid

        send = iq.send(timeout=30)
        xml = minidom.parseString(str(send))

        put_url = xml.getElementsByTagName('put')[0].firstChild.data

        if verify_ssl == 'False':
            req = requests.put(put_url, data=open(self.u_file, 'rb'),
                               verify=False)
        else:
            req = requests.put(put_url, data=open(self.u_file, 'rb'))

        log.info("Archivo subido en: " + put_url)
        print(put_url)


if __name__ == '__main__':

    if len(argv) > 1:
        config = []
        parser = configparser.SafeConfigParser()

        if len(argv) == 3:
            config = argv[2]

        else:
            path = '/'.join(abspath(__file__).split('/')[:-1]) + '/'
            config = path + 'config.ini'

        parser.read(config)

        jid = parser['Credentials']['jid']
        if parser['Credentials']['password'] == '':
            password = getpass.getpass()
        else:
            password = parser['Credentials']['password']

        component = parser['Technical']['component']
        verify_ssl = parser['Technical']['verify_ssl']
        max_size = parser['Technical']['max_size']

        bot = xmpp(jid, password, argv[1])

        if bot.connect():
            bot.process(block=False)

            if not component:
                log.info('Usando el discovery, puede que tarde un poco más de lo '
                      'normal, pero solo se hará la primera vez.')
                component = str(bot.discovery())

                if component:
                    xml = bot.disco_info(component)
                    xml = minidom.parseString(str(xml))
                    max_size = xml.getElementsByTagName('value'
                                                        )[1].firstChild.data

                elif not component:
                    component = jid.split('@')[1]

                    try:
                        xml = bot.disco_info(component)
                        xml = minidom.parseString(str(xml))
                        max_size = xml.getElementsByTagName('value'
                                                            )[1
                                                              ].firstChild.data

                    except:
                        log.error('Estás segura de que el servidor soporta HTTP Up'
                              'load? Si lo estás, ponte en contacto con el des'
                              'arrollador. Puedes ver como hacerlo en https://'
                              'daemons.cf/contacto.html')
                        bot.disconnect()
                        exit()

                # añadir la nueva configuración al fichero
                parser['Technical']['max_size'] = max_size
                parser['Technical']['component'] = component

                with open(config, 'w') as configfile:
                    parser.write(configfile)

                bot.upload(component, verify_ssl, max_size)

            else:
                bot.upload(component, verify_ssl, max_size)

            bot.disconnect()
        else:
            log.error('No me puedo conectar a xmpp.')
    else:
        log.error("No has pasado ningún archivo como argumento.")
