<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#orgheadline6">1. Simple Upload</a>
<ul>
<li><a href="#orgheadline1">1.1. Acerca de</a></li>
<li><a href="#orgheadline2">1.2. Instalación</a></li>
<li><a href="#orgheadline3">1.3. Configuración</a></li>
<li><a href="#orgheadline4">1.4. Uso</a></li>
<li><a href="#orgheadline5">1.5. Licencia</a></li>
</ul>
</li>
</ul>
</div>
</div>

# Simple Upload<a id="orgheadline6"></a>

## Acerca de<a id="orgheadline1"></a>

Este programa sirve para subir archivos mediante una cuenta XMPP de un servidor con el módulo HTTP Upload instalado. Hay varios servidores públicos con este módulo, tales como [<http://suchat.org>](http://suchat.org), [<http://otromundo.cf>](http://otromundo.cf) o [<http://elbinario.net>](http://elbinario.net).

## Instalación<a id="orgheadline2"></a>

Recomiendo usar un entorno virtual para usar **SimpleUpload**. Hay que instalar `virtualenv` para gestionar el entorno:

    aptitude install python3-virtualenv

Crear un entorno virtual y instalar los paquetes necesarios:

    mkdir ~/.env/
    virtualenv ~/.env/SimpleUpload/
    source ~/.env/SimpleUpload/bin/activate
    git clone https://daemons.cf/cgit/SimpleUpload/
    cd SimpleUpload
    pip3 install -r requirements.txt
    deactivate

Ahora crearemos un archivo que permita usar el entorno virtual sin tener que activarlo a mano. Creamos el archivo `/usr/local/bin/upload` con el siguiente contenido:

    #!/bin/bash

    source ~/.env/SimpleUpload/bin/activate
    python3 /ruta/al/repositorio/SimpleUpload/SimpleUpload.py $@
    deactivate

Le damos permiso de ejecución con `sudo chmod +x /usr/local/bin/upload` y ya se puede usar upload con normalidad.

## Configuración<a id="orgheadline3"></a>

Hay que editar el archivo `config.ini` en el directorio de git con las credenciales de la cuenta XMPP:

    cd /ruta/al/repositorio/SimpleUpload/
    cp example.ini config.ini
    nano config.ini

Los valores `jid` y `password` son nuestro usuario en formato **usuario@servidor.com** y la contraseña sin más. Si no se establece una contraseña, ésta será pedida durante la ejecución. El valor `verify_ssl` solo se debe establecer en **False** si el servidor web de que proporciona el módulo HTTP Upload usa un certificado inválido. El resto de valores no hace falta tocarlos, se estableceran solos en el primer uso.

## Uso<a id="orgheadline4"></a>

Se usa, con el archivo `/usr/local/bin/upload` que creamos antes, del siguiente modo:

    upload archivoASubir

No tiene mucho misterio. Si el archivo es demasiado grande o el servidor no soporta HTTP Upload (o eso piensa **SimpleUpload**) avisará.

## Licencia<a id="orgheadline5"></a>

    SimpleUpload is a cli tool to upload files to XMPP servers with HTTP Upload
    Copyright (C) 2016   <drymer@autistici.org>
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or (at
    your option) any later version.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
